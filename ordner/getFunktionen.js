function getGrafikenAttribute(xml) {
    
    
    let sty = [];

    var graphicIds = [];
    var graphicFiles = [];
    var graphicLefts = [];
    var graphicTops = [];
    var graphicTypes = [];
    var graphicArts = [];
    var graphicAccessibilitys = [];
    var graphicwidths = [];
    var graphicheights = [];
    var graphicbordercolors = [];
    var graphicbgcolor = [];
    var graphicrotation = [];
    var graphicborder = [];
    var graphicborderradius = [];
    var graphicrandlinks = [];
    var graphicrandrechts = [];
    var graphiczorder = [];
    var graphicrbeschreibung = [];
    var graphictexte = [];
    var graphicrlegenden = [];
    var graphicaktionen = [];

    var style = [];
    var returnparam1l = [];

    var tat = [];
    var param1L = [];
    var param2L = [];
    let seiteAblauf = [
        [],
        [],
        []
    ];


    $('#load').fadeOut();


    $(xml).find('graphic').each(function () {


        graphicId = $(this).attr("id");
        graphicIds.push(graphicId);
        graphicType = $(this).attr("type");
        graphicTypes.push(graphicType);
        graphicFile = $(this).attr("file");
        graphicFiles.push(graphicFile);
        graphicArt = $(this).attr("art");
        graphicArts.push(graphicArt);
        graphicAccessibility = $(this).attr("accessibility");
        graphicAccessibilitys.push(graphicAccessibility);
        graphicLeft = $(this).attr("left");
        graphicLefts.push(graphicLeft);
        graphicTop = $(this).attr("top");
        graphicTops.push(graphicTop);

        graphicw = $(this).attr("width");
        graphicwidths.push(graphicw);
        graphich = $(this).attr("height");
        graphicheights.push(graphich);
        /* <graphic id="bg_chapter_1" type="GraphicFreePos" file="images/bg_chapter_1.jpg" art="html" accessibility="true" 
        zorder="0" bordercolor="#c92629" bgcolor="#FFFFFF" rotate="1" border="10" borderradius="20" margin_left="2" margin_right="3">
        <description><![CDATA[hallo]]></description><alt><![CDATA[morgen]]></alt><legend><![CDATA[geld]]>
        </legend><event eventname="click">
                    <action actionname="psps"/>
                </event>
            </graphic>
        */

        graphicbc = $(this).attr("bordercolor");
        graphicbordercolors.push(graphicbc);
        graphiczo = $(this).attr("zorder");
        graphiczorder.push(graphiczo);
        graphicbgc = $(this).attr("bgcolor");
        graphicbgcolor.push(graphicbgc);
        graphicr = $(this).attr("rotate");
        graphicrotation.push(graphicr);
        graphicb = $(this).attr("border");
        graphicborder.push(graphicb);
        graphicbr = $(this).attr("borderradius");
        graphicborderradius.push(graphicbr);
        graphicml = $(this).attr("margin_left");
        graphicrandlinks.push(graphicml);
        graphicmr = $(this).attr("margin_right");
        graphicrandrechts.push(graphicmr);

        graphicbes = $(this).find("description").text();

        graphicrbeschreibung.push(graphicbes);


        graphicbes = $(this).find("alt").text();
        graphictexte.push(graphicbes);


        graphicbes = $(this).find("legend").text();
        graphicrlegenden.push(graphicbes);


        graphicbes = $(this).find("event").text();
        graphicaktionen.push(graphicbes);
    });


    // bassah


    //attributeF = $(this).attr("font-size");
    sty[0] = graphicIds;
    sty[1] = graphicTypes;
    sty[2] = graphicFiles;
    sty[3] = graphicArts;
    sty[4] = graphicAccessibilitys;
    sty[5] = graphicLefts;
    sty[6] = graphicTops;


    sty[7] = graphicbordercolors;
    sty[8] = graphiczorder;
    sty[9] = graphicbgcolor;
    sty[10] = graphicrotation;
    sty[11] = graphicborder;
    sty[12] = graphicborderradius;
    sty[13] = graphicrandlinks;
    sty[14] = graphicrandrechts;


    sty[15] = graphicrbeschreibung;
    sty[16] = graphictexte;
    sty[17] = graphicrlegenden;
    sty[18] = graphicaktionen;



    for (var i = 0; i < graphicIds.length; i++) {

        style[i] = " position:absolute;left:" + graphicLefts[i] + "px;top:" + graphicTops[i] + "px;width:" + graphicwidths[i] + "px;height:" + graphicheights[i] + "px;border-width:" + graphicborder[i] + "px;border-radius:" + graphicborderradius[i] + "px;border-color:"
            + graphicbordercolors[i] + ";background-color:" + graphicbgcolor[i] + ";z-index:" + graphiczorder[i] + ";transform:rotate(" + graphicrotation[i] + "deg);"
    }
    sty[19] = style;
    //<graphic id="g1003a" type="GraphicFreePos" file="images/i1001a.png" art="html" accessibility="true" left="776" top="152">

    // returnparam1l = getAblauf("xml", function () { });
    // console.log(returnparam1l);
    console.log(sty);
    if (sty[0]) {
        console.log("ja es gibt Grafiken auf die Seite");
        return sty;

    } else { return null }
}






function getTexteAttribute(xml) {
    var text;
    var texte = [];
    var texteclassen = [];

    var texteids = [];
    var texteinhalte = [];
    var textTypen = [];
    var textLefts = [];
    var textTops = [];
    var textwidths = [];

    var textausrichtung = [];
    var texteingabefelder = [];
    var textheights = [];
    var textmaxwidths = [];
    var textVausrichtung = [];
    var textabstand = [];

    var textborder = [];
    var textborderradius = [];
    var textrahmenfarbe = [];
    var textHfarbe = [];
    var textSfarbe = [];
    var textSgroesse = [];

    var textPausrichtung = [];
    var textzorder = [];
    var textdecke = [];
    var textschatten = [];
    var textrotation = [];
    var textspitze = [];
    var textspitzeAnschluss = [];
    var textspitzeABg = [];
    var style = [];




    var textTitle = "TextTitle";
    var textleft = "TextScreenLeft";

    var TextScreenLeft = "  position:absolute; left:150px;  top:100px;  width:467px;  font-weight: normal; line-height:120%;  border:dotted 0px #FF00FF; z-index:80; "

    var TextTitle = " position:	absolute;left:		150px;top:		144px;line-height:120%;color:#333333;z-index: 500;   "



    $('#load').fadeOut();
    $(xml).find('text').each(function () {

        text = $(this).text();
        texteinhalte.push(text);
        var textType = $(this).attr("type");
        textTypen.push(textType);
        var textclass = $(this).attr("type");
        texteclassen.push(textclass);
        var textid = $(this).attr("id");
        texteids.push(textid);
        var width = $(this).attr("width");
        textwidths.push(width);
        var top = $(this).attr("top");
        textTops.push(top);
        var tleft = $(this).attr("left");
        textLefts.push(tleft);


        var textausr = $(this).attr("textalign");
        textausrichtung.push(textausr);
        var textef = $(this).attr("accessibility");
        texteingabefelder.push(textef);
        var theight = $(this).attr("height");
        textheights.push(theight);
        var textvaus = $(this).attr("textvalign");
        textVausrichtung.push(textvaus);
        var tmwidth = $(this).attr("maxwidth");
        textmaxwidths.push(tmwidth);
        var abs = $(this).attr("padding");
        textabstand.push(abs);



        var tr = $(this).attr("border");
        textborder.push(tr);
        var trr = $(this).attr("borderradius");
        textborderradius.push(trr);
        var trf = $(this).attr("bordercolor");
        textrahmenfarbe.push(trf);
        var thf = $(this).attr("bgcolor");
        textHfarbe.push(thf);
        var tsf = $(this).attr("fontcolor");
        textSfarbe.push(tsf);
        var tsg = $(this).attr("fontsize");
        textSgroesse.push(tsg);



      
        var textpaus = $(this).attr("float");
        textPausrichtung.push(textpaus);
        var textzor = $(this).attr("zorder");
        textzorder.push(textzor);
        var td = $(this).attr("opacity");
        textdecke.push(td);
        var tsc = $(this).attr("shadow");
        textschatten.push(tsc);



        var textro = $(this).attr("rotate");
        textrotation.push(textro);
        var textspi = $(this).attr("arrowweight");
        textspitze.push(textspi);
        var textsAnsch = $(this).attr("bubblealign");
        textspitzeAnschluss.push(textsAnsch);
        var textsAbg = $(this).attr("bubblevalign");
        textspitzeABg.push(textsAbg);


        texte[0] = texteids;
        texte[1] = texteinhalte;
        texte[2] = textTypen;
        texte[3] = textLefts;
        texte[4] = textTops;
        texte[5] = textwidths;


        texte[6] = textausrichtung;
        texte[7] = texteingabefelder;
        texte[8] = textheights;
        texte[9] = textmaxwidths;
        texte[10] = textVausrichtung;
        texte[11] = textabstand;

        texte[12] = textborder;
        texte[13] = textborderradius;
        texte[14] = textrahmenfarbe;
        texte[15] = textHfarbe;
        texte[16] = textSfarbe;
        texte[17] = textSgroesse;



        texte[18] = textPausrichtung;
        texte[19] = textzorder;
        texte[20] = textdecke;
        texte[21] = textschatten;
        texte[22] = textrotation;
        texte[23] = textspitze;
        texte[24] = textspitzeAnschluss;
        texte[25] = textspitzeABg;
        texte[26] = texteclassen;

    });
    console.log(textTypen);
    console.log(texteclassen);

    for (var i = 0; i < textTypen.length; i++) {
        const currentString = textTypen[i];
        if (currentString === "TextTitle") {
            if (textwidths[i]) {
                textTypen[i] = " position:	absolute;left:	155px;top:	144px;width:" + textwidths[i] + "px;  line-height:120%;color:#333333;z-index: 500;   ";
            } else { textTypen[i] = TextTitle; }


        } else if (currentString === "TextScreenLeft") {
            if (textwidths[i]) {
                textTypen[i] = "position:absolute;left:30px;top:100px; width:" + textwidths[i] + "px; font-weight: normal;line-height:120%; border:dotted0px #FF00FF;z-index:80; ";
            } else {
                textTypen[i] = TextScreenLeft;
            }
        } else {
            textTypen[i] = "position:absolute;left:" + textLefts[i] + "px;top:" + textTops[i] + "px; width:" + textwidths[i] + "px; font-weight: normal;line-height:120%; border:dotted0px #FF00FF;z-index:80; ";
            //textTypen[i];

        }

    }
    for (var i = 0; i < texteids.length; i++) {
        style[i] = " position:absolute;left:" + textLefts[i] + "px;top:" + textTops[i] + "px;width:" + textwidths[i] + "px;height:" + textheights[i] + "px;border-width:" + textborder[i] + "px;border-radius:" + textborderradius[i] + "px;border-color:"
            + textrahmenfarbe[i] + ";background-color:" + textHfarbe[i] + ";color:" + textSfarbe[i] + ";font-size:" + textSgroesse[i] + "px;float:" + textPausrichtung[i] + ";z-index:" + textzorder[i] + ";opacity:" + textdecke[i] + ";text-shadow:" + textschatten[i] + "px:transform:rotate(" + textrotation[i] + "deg);padding:"
            + textabstand[i] + ";text-aling:" + textausrichtung[i] + ";vertical-align:" + textVausrichtung[i] + ";line-height:110%;"
    }
    texte[27] = style;
    console.log(textTypen);
    console.log(texte[0]);
    if (texte[0]) {
        console.log(texte); return texte;
    }
    else { return null; }


}
